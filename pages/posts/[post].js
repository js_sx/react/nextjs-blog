import Layout from '../../components/layout';
import { getAllPostIds, getPostData } from '../../lib/posts';
import Head from 'next/head';
import { useRouter } from 'next/router';



export default function Post({ postData }) {
  const router = useRouter();

  if (router.isFallback) {
    return <div>loading...</div>
  }

  return (
    <Layout>
      <Head>
        <title>{postData.title}</title>
      </Head>
      {postData.title}
      <br />
      {postData.id}
      <br />
      {postData.date}
      <div dangerouslySetInnerHTML={{ __html: postData.contentHtml }} />
    </Layout>
  );
}


export async function getStaticPaths() {
  return {
    paths: getAllPostIds(),
    fallback: true,
  };
}

export async function getStaticProps({ params }) {
  const postData = await getPostData(params.post);
  return {
    props: {
      postData,
    },
  };
}